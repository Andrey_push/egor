/*
WebFont.load({
    google: {
        families: ['Ubuntu:300,700:cyrillic']
    }
});
*/
$(document).ready( function() {
	$('.phone-mask').mask("+7(999) 999-9999");

	$('.header-burger').click(function(event) {
		$('.nav').addClass('open');
		$('.overlay').addClass('active');
	});

	$('.hamburger-close').click(function(event) {
		$('.nav').removeClass('open');
		$('.overlay').removeClass('active');
	});

	$('.overlay').click(function(event) {
		$('.nav').removeClass('open');
		$('.overlay').removeClass('active');
	});

	$('.economy-slider').owlCarousel({
		items: 1,
		smartSpeed: 1000,
	  loop: true,
	  lazyLoad: true,
	  lazyContent: true,
	  dots: false,
	  responsive: {
      0 : {
      	dots: true,
      },
      1200 : {
        dots: false
      },
	  }
	});

	$(".economy-slider-nav--prev").click(function(e){
	  $('.economy-slider').trigger("prev.owl.carousel");
	});
	$(".economy-slider-nav--next").click(function(e){
	  $('.economy-slider').trigger("next.owl.carousel");
	});

	function tabs(){
		$('.tab-content:not(":first")').addClass('hidden'); //прячем все кроме первого
		$('.tab-menu .tab-menu__item').on('click', function(){
			var dataTab = $(this).data('tab');
			var getWrapper = $(this).closest('.tab-wrapper');

			getWrapper.find('.tab-menu .tab-menu__item').removeClass('active'); //удаляем всем класс актив
			$(this).addClass('active'); // присваеваем, куда кликнули

			$('.tab-content').addClass('hidden'); // все прячем
			$('.tab-content[data-tab='+dataTab+']').removeClass('hidden');// показываем, согласно дата атрибуту того, куда кликнули
		});
	};
	tabs();

	$('.object-menu-mobile').click(function(event) {
		$('.our-object-menu').slideToggle();
	});
	$('.our-object-menu .our-object-menu__item').click(function(event) {
		if ($(window).width() > 768) {return false;}
		var text = $(this).text();
		$('.object-menu-mobile .object-menu-mobile__text').text(text);
		$('.our-object-menu').slideUp();
	});
	/*$(window).resize(function(event) {
		if ( ($(window).width() > 768) && ($(window).width() < 992) ) {
			$('.our-object-menu').css('display', 'flex');
		}
		else if ( $(window).width() > 992 ) {
			$('.our-object-menu').css('display', 'block');
		}
		else if($(window).width() > 768){
			$('.our-object-menu').css('display', 'none');
		}
	});*/

	/*Stage-tabs*/
		function stageTabs(){

		};
		stageTabs();
		/*var show2 = true;
    var stageBox = ".our-number__nubmer";
    $(window).on("scroll load resize", function () {
        if (!show) return false; // Отменяем показ анимации, если она уже была выполнена
        var w_top = $(window).scrollTop(); // Количество пикселей на которое была прокручена страница
        var e_top = $(stageBox).offset().top; // Расстояние от блока со счетчиками до верха всего документа
        var w_height = $(window).height(); // Высота окна браузера
        var d_height = $(document).height(); // Высота всего документа
        var e_height = $(stageBox).outerHeight(); // Полная высота блока со счетчиками
        if (w_top + 500 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height) {
       		
         show2 = false;
        }
    });*/
	/*-- End of Stage-tabs*/

	/*Counter*/
		var show = true;
    var countbox = ".our-number__nubmer";
    $(window).on("scroll load resize", function () {
        if (!show) return false; // Отменяем показ анимации, если она уже была выполнена
        var w_top = $(window).scrollTop(); // Количество пикселей на которое была прокручена страница
        var e_top = $(countbox).offset().top; // Расстояние от блока со счетчиками до верха всего документа
        var w_height = $(window).height(); // Высота окна браузера
        var d_height = $(document).height(); // Высота всего документа
        var e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
        if (w_top + 500 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height) {
       		$('.our-number__nubmer').each(function () {
       			console.log();
       	    $(this).prop('Counter',0).animate({
       	        Counter: parseInt($(this).attr('data-count')),
       	    }, {
       	        duration: 2500,
       	        easing: 'swing',
       	        step: function (now) {
                   $(this).text(Math.ceil(now));
       	        }
       	    });
       		});
         show = false;
        }
    });
	/*-- End ofCounter*/

	/*Scroll*/
		$(".menu li").click(function(){
      var _href = $(this).attr("data-yakor");
      $("html, body").animate({scrollTop: $(_href).offset().top+"px"},1000);
      $('.nav').removeClass('open');
      $('.overlay').removeClass('active');
      return false;
    });
	/*-- End of Scroll*/


	// tabs //
	$('.stage-menu-item').on('click', function(){
		$('.stage-content_wrapper .stage-content').fadeOut(0).eq($(this).index()).fadeIn(300);
		$('.stage-menu-item').removeClass('active');
		var item = $(this).index('.stage-menu-item') + 1;

		for(var i = 0; i < item; i++){
			$('.stage-menu-item').eq(i).addClass('active');
		}
	});


});