/*
 *
 * Определяем переменные
 *
 */

var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    csso = require('gulp-csso'),
    autoprefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    csscomb = require('gulp-csscomb'),
    gutil = require('gulp-util'),
    svgSprite = require('gulp-svg-sprites'),
    plumber = require('gulp-plumber'),
    connect = require('gulp-connect'),
    livereload = require('gulp-livereload'),
    ftp = require('gulp-ftp');
    const $ = require('gulp-load-plugins')();

/*
 *
 * Создаем задачи (таски)
 *
 */
gulp.task('connect', function() {
    connect.server({
        livereload: true,
        port: 8080
    });
});


gulp.task('sass', function () {
    gulp.src('./assets/sass/style.scss')
        .pipe($.sass())
        .on('error', $.notify.onError())
        //.pipe($.autoprefixer(['last 15 versions', '>1%', 'ie 8', 'ie 7'], {cascade: true}))
        .pipe(csscomb())
        .pipe(csso())
        .pipe(gulp.dest('./assets/css/'))
        .pipe(connect.reload());
});

gulp.task('js', function() {
    gulp.src([
        './assets/js/libs/jquery.js',
        './assets/js/libs/phoneMask.js',
        './assets/js/libs/owl.carousel.js',
        './assets/js/libs/jquery.fancybox.min.js',
        './assets/js/script.js'
    ])
        .pipe(concat('min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./assets/js/'))
        .pipe(connect.reload());
});


gulp.task('ftp', function () {
    return gulp.src([
        '*.html',
        '**/*',
        '!public/**/*',
        '!node_modules/**',
        '!bower_components/**',
        '!frontend-blank-V2/**',
        '!psd/**',
        '!nevatrip/**',
        '!.gitignore',
        '!gulpfile.js',
        '!package.json'
    ])
        .pipe(ftp({
            host: 'ftp.apexlife.ru',
            user: 'User Name',
            pass: 'Password',
            remotePath: '/'
        }))

        .pipe(gutil.noop());
});


gulp.task('svg', function () {
    return gulp.src('assets/img/svg/**/*.svg')
        .pipe(svgSprite())
        .pipe(gulp.dest("assets/svg/"));
});

gulp.task('html', function() {
    gulp.src('*.html')
        .pipe(connect.reload());
});

gulp.task('watch', function () {
    gulp.watch('./assets/sass/**/*.scss', ['sass']);
    gulp.watch('./assets/js/script.js', ['js']);
    gulp.watch('*.html', ['html']);
});

gulp.task('con', ['connect', 'watch']);